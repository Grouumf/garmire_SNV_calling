from setuptools import setup, find_packages
import sys, os

VERSION = '0.1.7'

setup(name='garmire_SNV_calling',
      version=VERSION,
      description="compute SNV from RNA-seq following GATK recommendations",
      long_description="""""",
      classifiers=[],
      keywords='SNV calling RNA-seq; single-cells',
      author='Olivier Poirion (PhD)',
      author_email='opoirion@hawaii.edu',
      url='',
      license='MIT',
      packages=find_packages(exclude=['examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[],
      )
